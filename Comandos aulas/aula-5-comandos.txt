PUT empregados/_doc/1
{
  "matricula": 1,
  "nome": "Eduardo",
  "salario": 1232.00,
  "data_contratacao": "10/10/2008",
  "departamento": "TI"
}

GET /empregados/_doc/1

GET /empregados/_doc/2

GET /empregados/_doc/1/_source

HEAD /empregados/_doc/1

PUT /empregados/_doc/1
{
  "matricula": 1,
  "nome": "Eduardo",
  "salario": 1932.00,
  "data_contratacao": "10/10/2008",
  "departamento": "TI"
}

POST /empregados/_doc/1/_update
{
  "doc": {
    "nome": "sssEduardo"
  }
}

DELETE /empregados/_doc/1/

GET /empregrados/

DELETE /empregados
