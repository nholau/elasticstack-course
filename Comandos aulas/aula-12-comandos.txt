Aula 12
GET /carros/_search
{
  "query": {
    "match_all": {}
  }
}

GET /carros/_search
{
  "size": 20,
  "query": {
    "match_all": {}
  }
}

GET /carros/_search
{
  "from": 0,
  "size": 5,
  "query": {
    "match_all": {}
  }
}

GET /carros/_search
{
  "from": 0,
  "size": 5,
  
  "query": {
    "match_all": {}
  },
  
  "sort": [
      { "preco": { "order": "desc" }} 
    ]
}

GET /carros/_search
{
 
  "query": {
    "match": { "marca": "dodge"}
  }
  
}

GET /carros/_search
{

  "aggs": {
    "carros_populares": {
      "terms": {
        "field": "marca.keyword",
        "size": 10
      }
    }
  } 

}

GET /carros/_search
{
  "aggs": {
    "carros_populares": {
      "terms": {
        "field": "marca.keyword",
        "size": 10
      },
      "aggs": {
        "preco_medio": {
          "avg": {
            "field": "preco"
          }
        },
        "preco_maximo": {
          "max": {
            "field": "preco"
          }
        },
        "preco_minimo": {
          "min": {
            "field": "preco"
          }
        }
      }
    }
  } 
}

GET /carros/_search
{
  "query": {
    "match": {
      "cor": "vermelho"
    }
  },
  "aggs": {
    "carros_populares": {
      "terms": {
        "field": "marca.keyword",
        "size": 10
      },
      "aggs": {
        "preco_medio": {
          "avg": {
            "field": "preco"
          }
        },
        "preco_maximo": {
          "max": {
            "field": "preco"
          }
        },
        "preco_minimo": {
          "min": {
            "field": "preco"
          }
        }
      }
    }
  } 
}

GET /carros/_search
{
  "aggs": {
    "carros_populares": {
      "terms": {
        "field": "marca.keyword",
        "size": 10
      },
      "aggs": {
        "stats_preco": {
          "stats": {
            "field": "preco"
          }
        }
      }
    }
  } 
}

GET /carros/_search
{
  "aggs": {
    "carros_populares": {
      "terms": {
        "field": "marca.keyword",
        "size": 10
      },
      "aggs": {
        "vendido_em": {
          "range": {
            "field": "vendido",
            "ranges": [
              { 
                "from": "2016-01-01",
                "to": "2016-05-18"
              },
              { 
                "from": "2016-05-18",
                "to": "2017-01-01"
              }
            ]
          }
        }
      }
    }
  } 
}

GET /carros/_search
{
  "aggs": {
    "carros_populares": {
      "terms": {
        "field": "marca.keyword",
        "size": 10
      },
      "aggs": {
        "vendido_em": {
          "range": {
            "field": "vendido",
            "ranges": [
              { 
                "from": "2016-01-01",
                "to": "2016-05-18"
              },
              { 
                "from": "2016-05-18",
                "to": "2017-01-01"
              }
            ]
          },
          "aggs": {
            "valor_medio": {
              "avg": {
                "field": "preco"
              }
            }
          }
        }
      }
    }
  } 
}

GET /carros/_search
{
  "aggs": {
    "carros_populares": {
      "terms": {
        "field": "condicao.keyword",
        "size": 10
      }
    }
  } 
}

GET /carros/_search
{
  "aggs": {
    "carros_populares": {
      "terms": {
        "field": "condicao.keyword",
        "size": 10
      },
      "aggs": {
        "preco_medio": {
          "avg": {
            "field": "preco"
          }
        }
      }
    }
  } 
}

GET /carros/_search
{
  "aggs": {
    "carros_populares": {
      "terms": {
        "field": "condicao.keyword",
        "size": 10
      },
      "aggs": {
        "preco_medio": {
          "avg": {
            "field": "preco"
          }
        },
        "marca": {
          "terms": {
            "field": "marca.keyword",
            "size": 10
          }
        }
      }
    }
  } 
}

GET /carros/_search
{
  "size": 0,
  "aggs": {
    "carros_populares": {
      "terms": {
        "field": "condicao.keyword",
        "size": 10
      },
      "aggs": {
        "preco_medio": {
          "avg": {
            "field": "preco"
          }
        },
        "marca": {
          "terms": {
            "field": "marca.keyword",
            "size": 10
          },
          "aggs": {
            "min_preco": {
              "min": {
                "field": "preco"
              }
            },
            "max_price": {
              "max": {
                "field": "preco"
              }
            }
          }
        }
      }
    }
  } 
}